package com.maxima.Api.resources;

import com.maxima.Api.domain.Cliente;
import com.maxima.Api.domain.Pedido;
import com.maxima.Api.service.ClienteService;
import com.maxima.Api.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/clientes")
public class ClienteResource {

    @Autowired
    private ClienteService service;

    @GetMapping(value ="/{id}")
    public ResponseEntity<?> find(@PathVariable String id){
        Cliente cliente = service.findById(id);
        return ResponseEntity.ok().body(cliente);
    }

    @GetMapping
    public ResponseEntity<List<Cliente>> findAll(){
        List<Cliente> pedidos = service.findAll();
        return ResponseEntity.ok().body(pedidos);
    }
}