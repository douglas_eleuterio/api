package com.maxima.Api.resources;

import com.maxima.Api.domain.Produto;
import com.maxima.Api.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/produtos")
public class ProdutoResource {

    @Autowired
    private ProdutoService service;

    @GetMapping(value ="/{id}")
    public ResponseEntity<?> find(@PathVariable String id){
        Produto produto = service.find(id);
        return ResponseEntity.ok().body(produto);
    }

    @GetMapping
    public ResponseEntity<List<Produto>> findAll(){
        List<Produto> produtos = service.findAll();
        return ResponseEntity.ok().body(produtos);
    }
}