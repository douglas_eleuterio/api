package com.maxima.Api.service.exceptions;

public class ObjetoNaoEncontradoException extends RuntimeException{
    private static final Long servialVersionUID = 1L;

    public ObjetoNaoEncontradoException(String message) {
        super(message);
    }

    public ObjetoNaoEncontradoException(String message, Throwable cause) {
        super(message, cause);
    }
}
