package com.maxima.Api.service;

import com.maxima.Api.domain.Pedido;
import com.maxima.Api.repository.PedidoRepository;
import com.maxima.Api.service.exceptions.ObjetoNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository repo;

    @Autowired
    ProdutoService produtoService;

    @Transactional
    public Pedido findById(String id){
        Optional<Pedido> obj = repo.findById(id);
        return obj.orElseThrow( () -> new ObjetoNaoEncontradoException(
                "Pedido não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName()));
    }

    public List<Pedido> findAll(){
        return repo.findAll();
    }

    @Transactional
    public void insert(Pedido obj){
        repo.save(obj);
    }

}
