package com.maxima.Api.service;

import com.maxima.Api.domain.Produto;
import com.maxima.Api.repository.ProdutoRepository;
import com.maxima.Api.service.exceptions.ObjetoNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    ProdutoRepository repo;

    public Produto find(String id){
    Optional<Produto> obj =  repo.findById(id);
    return obj.orElseThrow(() -> new ObjetoNaoEncontradoException(
            "Produto Não Encontrado! Id: " + id + ", Tipo: " + Produto.class.getName()));
    }

    public List<Produto> findAll(){
        return repo.findAll();
    }
}
