package com.maxima.Api.service;

import com.maxima.Api.domain.Cliente;
import com.maxima.Api.domain.Pedido;
import com.maxima.Api.repository.ClienteRepository;
import com.maxima.Api.service.exceptions.ObjetoNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repo;

    @Transactional
    public Cliente findById(String id){
        Optional<Cliente> obj = repo.findById(id);
        return obj.orElseThrow( () -> new ObjetoNaoEncontradoException(
                "Cliente não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName()));
    }


    public List<Cliente> findAll(){
        return repo.findAll();
    }
}
