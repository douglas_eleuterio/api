CREATE TABLE IF NOT EXISTS produto(
    id varchar not null unique ,
    codigo varchar unique not null,
    nome varchar not null,
    preco_unitario double precision,
    imagem_url varchar default '',
    constraint produto_pk primary key (id)
);