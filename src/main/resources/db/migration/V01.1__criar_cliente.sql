CREATE TABLE IF NOT EXISTS cliente(
    id varchar not null unique ,
    codigo varchar,
    nome varchar,
    constraint cliente_pk primary key (id)
);