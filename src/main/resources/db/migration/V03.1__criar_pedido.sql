CREATE TABLE IF NOT EXISTS pedido(
    id varchar not null unique ,
    valor_total double precision,
    valor_frete double precision,
    cliente_id varchar NOT NULL,
    quantidade_de_itens int not null ,
    constraint pedido_pk primary key (id)
);