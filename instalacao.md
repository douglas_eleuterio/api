## PoC do Processo Seletivo da MáximaTech 

### Recursos utilizados para construção do sistema/e-commerce.

 * Postgres - Banco de dados Relacional
 * Java 8 - Linguagem de Programação
 * Angular 8 - Framework de desenvolvimento web, composto por Typescript, HTML e CSS.
 * String Boot - Framework de desenvolvimento
 * Maven - Gerenciador de Dependencias.
 * Flyway - Gerenciador de Versionamento do Banco de dados.
 * Spring Data - Framework para manipulação de dados e ponte de comunicação entre aplicação e banco de dados.
 
##Para instalação/execução do sistema você deverá ter instalado previamente os sequintes sofwares.

 * NPM 
 * Angular CLI
 * Java JDE/JDK na versão 8 ou superior.
 * PostgreSQL 12 ou Superior.
 * IDE de desenvolvimento Java(Foi utilizado IntelliJIDEA para desenvolvimento)
 
###Instruções para execução

1. Acessar o PostreSQL e criar um banco chamado 'maximatech' (as tabelas são gerenciadas pela aplicação 'API').
 * Caso prefira, é possível utilizar qualquer outro nome para o banco de dados, mas lembre-se, **deve-se alterar o nome do banco e as credenciais de acesso no arquivo application.properties contido no projeto localizado em '~/Api/src/main/resources'**

2. Com auxilio da IDE, abrir o projeto com nome de 'API', realizar o import do Maven e posteriormente execução/start da aplicação.
 * É possível utilizar ferramenta de linha de comando caso prefira, para isso, consulte o tutorial em https://www.vogella.com/tutorials/ApacheMaven/article.html 
3. Após a execução da API iremos executar a aplicação 'e-commerce', para isso, após realizado o download e acessado a pasta do projeto, abrir o terminal, navegar para a pasta do projeto, executar o comando **npm install**, após finalizado, executar o comando **ng serve**.
 * Nesse ponto será possível acessar a aplicação em http://localhost:4200 caso não altere a porta padrão.  
 
##Dúvidas quanto a instalação/execução, basta solicitar ajuda no número 62 98560-6521.

