import {Component, OnInit} from '@angular/core';
import {ClienteService} from '../core/services/cliente.service';
import {Cliente} from '../core/model/cliente';
import {Produto} from '../core/model/produto';
import {Pedido} from '../core/model/pedido';
import {PedidoService} from '../core/services/pedido.service';
import {FreteService} from '../core/services/frete.service';
import {ProdutoService} from '../core/services/produto.service';

@Component({
  selector: 'app-novo-pedido',
  templateUrl: './novo-pedido.component.html',
  styleUrls: ['./novo-pedido.component.scss']
})
export class NovoPedidoComponent implements OnInit {

  clientes: Cliente[] = [];
  clienteSelecionado: Cliente;

  produtos: Produto[] = [];

  produtosSelecionado: Produto[] = [];
  produtoSelecionado: Produto;

  totalItens = 0;
  totalFrete = 0;
  quantidadeTotalDeItens = 0;
  totalPedido = 0;
  fretePedido = 0;

  constructor(private clienteServices: ClienteService,
              private pedidoServices: PedidoService,
              private freteServices: FreteService,
              private produtoService: ProdutoService) { }

  ngOnInit() {
    this.clienteServices.listarCliente().subscribe((resp: Cliente[]) => {
      this.clientes = resp;
    });
    this.produtoService.listarProdutos().subscribe((resp: Produto[]) => {
      this.produtos = resp;
    });
  }

  public selecionarProduto(produto: Produto) {
    let esteProdutoEstaNoCarrinho = false;
    this.produtosSelecionado.map((item) => {
      if ( item.codigo === produto.codigo) {
        esteProdutoEstaNoCarrinho = true;
      }
    });
    if ( !esteProdutoEstaNoCarrinho ) {
      this.produtosSelecionado.push(produto);
    }
    this.quantidadeTotalDeItens += 1;
    this.totalItens += Number(produto.precoUnitario.toFixed(2));
    this.fretePedido += this.numeroAleatorio(5, 10 * this.quantidadeTotalDeItens);
    this.totalFrete += this.fretePedido;
    this.totalPedido = Number((this.totalItens + this.totalFrete).toFixed(2));
    this.produtoSelecionado = new Produto();
  }

  public esvaziarCarrinho() {
    this.produtosSelecionado = [];
    this.totalItens = 0;
    this.clienteSelecionado = new Cliente();
    this.quantidadeTotalDeItens = 0;
    this.totalFrete = 0;
    this.totalPedido = 0;
  }

  public finalizarPedido() {
    let pedido = new Pedido();
    pedido.cliente = this.clienteSelecionado;
    pedido.valorTotal = this.totalItens;
    pedido.quantidadeDeItens = this.quantidadeTotalDeItens;
    pedido.valorFrete = this.fretePedido;
    pedido.valorTotal = pedido.valorTotal + pedido.valorFrete;

    this.pedidoServices.salvar(pedido).subscribe((resp) => {
    });
    this.esvaziarCarrinho();
    confirm('Pedido salvo com sucesso!');
  }

  public removeProduto(item) {
    this.produtosSelecionado.splice(this.produtosSelecionado.indexOf(item), 1);
    this.totalItens = 0;
    this.totalFrete = 0;
    this.quantidadeTotalDeItens = 0;
  }

  public numeroAleatorio(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }
}
