import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {URL_API} from 'src/app/app.api';


@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  constructor(private http: HttpClient) { }

  public listarProdutos(parametro?){
    return this.http.get(`${URL_API}/produtos${parametro || ''}`);
  }
}
