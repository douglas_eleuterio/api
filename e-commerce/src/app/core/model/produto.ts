export class Produto {
    id: string;
    codigo: string;
    nome: string;
    precoUnitario: number;
    quantidade: number;
    imagemUrl: string;
}
