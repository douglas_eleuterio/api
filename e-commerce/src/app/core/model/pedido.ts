import {Cliente} from './cliente';

export class Pedido {
    id: number;
    quantidadeDeItens: number;
    valorTotal: number;
    valorFrete: number;
    cliente: Cliente;
}
