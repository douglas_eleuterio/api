import {ProdutoDTO} from './produtoDTO';

export class ItensDTO {
  quantidade: string;
  produto: ProdutoDTO;
}
