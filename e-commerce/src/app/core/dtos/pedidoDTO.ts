import {ClienteDTO} from './clienteDTO';
import {ProdutoDTO} from './produtoDTO';

export class PedidoDTO {
  cliente: ClienteDTO;
  itensDto: Array<ProdutoDTO>[];
  valorTotal: number;
}
